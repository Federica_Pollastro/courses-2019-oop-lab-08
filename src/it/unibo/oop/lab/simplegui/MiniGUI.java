/**
 * 
 */
package it.unibo.oop.lab.simplegui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.*;
import javax.swing.BoxLayout;



public class MiniGUI {

    private static final String TITLE = "A very simple GUI application";
    private static final int PROPORTION = 6;
    private final Random rng = new Random();
    private final JFrame frame = new JFrame(TITLE);
    /**
     * 
     */
    public MiniGUI() {
        final JPanel canvas = new JPanel();
        final JPanel canvas1 = new JPanel();

        canvas.setLayout(new BorderLayout());
        canvas1.setLayout(new BoxLayout(canvas1, BoxLayout.X_AXIS));

        final JButton write = new JButton("Print a random  number con standard output");
        final  JTextField tes = new JTextField("Result", PROPORTION);
        canvas.add(canvas1, BorderLayout.CENTER);
        canvas1.add(write, BorderLayout.CENTER);
     //   canvas1.add(new JTextField("Result", PROPORTION));
        canvas.add(tes , BorderLayout.NORTH);

        frame.setContentPane(canvas);
        frame.getContentPane().add(canvas1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        write.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
               tes.setText(Integer.toString(rng.nextInt()));
                
            }
        });
}

    private void display() {
        
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        frame.pack();
        /*
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / PROPORTION, sh / PROPORTION);
        */

        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

    public static void main(final String... args) {
       new MiniGUI().display();
    }

}
